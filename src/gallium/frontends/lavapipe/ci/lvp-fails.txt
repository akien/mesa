# Vertex 0: Expected red, got (-0.372549, -0.372549, -0.372549, -0.372549) (Fail)
# Likely fixed by VK-GL-CTS a3ac66a77e14 ("Zero initialize XFB counter buffer")
dEQP-VK.rasterization.provoking_vertex.transform_feedback.first.line_strip_with_adjacency,Fail
dEQP-VK.rasterization.provoking_vertex.transform_feedback.per_pipeline.triangle_strip_with_adjacency,Fail

#full list
dEQP-VK.api.driver_properties.conformance_version,Fail
dEQP-VK.fragment_operations.early_fragment.sample_count_early_fragment_tests_depth_samples_4,Fail
dEQP-VK.glsl.crash_test.divbyzero_comp,Crash
dEQP-VK.glsl.texture_functions.query.texturequerylod.isamplercube_zero_uv_width_fragment,Fail
dEQP-VK.glsl.texture_functions.query.texturequerylod.samplercube_float_zero_uv_width_fragment,Fail
dEQP-VK.glsl.texture_functions.query.texturequerylod.samplercubeshadow_zero_uv_width_fragment,Fail
dEQP-VK.glsl.texture_functions.query.texturequerylod.usamplercube_zero_uv_width_fragment,Fail

